from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    url(r'^a/', include('webservice.urls')),
    url(r'^admin/', admin.site.urls),
]
