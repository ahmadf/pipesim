# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import os

from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from celery.task.control import revoke

from .tasks import calculate_pipeline


@csrf_exempt
def run_pipeline(request):
    body_unicode = request.body.decode('utf-8')
    body = json.loads(body_unicode)

    res = calculate_pipeline.delay(body)

    return JsonResponse({'id': res.id})


def check_status(request):
    task_id = request.GET['id']
    with open(task_id + '.out', 'r') as fp:
        content = [x.strip() for x in fp.readlines()]
    return JsonResponse({'lines': content})


def get_result(request):
    task_id = request.GET['id']
    result = calculate_pipeline.AsyncResult(task_id)
    if result.state != 'SUCCESS' and result.state != 'FAILURE':
        return JsonResponse({'error': {'name': 'not_finished_yet', 'params': ''}}, status=404)
    return JsonResponse(result.get())


def terminate_job(request):
    task_id = request.GET['id']
    result = calculate_pipeline.AsyncResult(task_id)
    if result.state != 'SUCCESS' and result.state != 'FAILURE':
        revoke(task_id, terminate=True)
        resp = {'status': 'terminated'}
    else:
        resp = {'status': 'allready_finished'}
    os.remove(task_id + '.out')
    return JsonResponse(resp)
