from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^run-pipeline/$', views.run_pipeline),
    url(r'^check-status/$', views.check_status),
    url(r'^get-result/$', views.get_result),
    url(r'^terminate-job/$', views.terminate_job),
]
