# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task

from pipe_steady.models import GasProperty, PipeLine, Settings, Logger
from pipe_steady.engine import SteadySolver


@shared_task(bind=True)
def calculate_pipeline(self, body):
    points = body['points']
    streams = body['pipes']
    opt = body.get('settings', {})

    setting = Settings(method=body['method'], mod_factor=float(opt['constants']['cf']), temperature_unit=opt['units']['temperature_unit'],
                       pressure_unit=opt['units']['pressure_unit'], length_unit=opt['units']['length_unit'],
                       diameter_unit=opt['units']['diameter_unit'], flow_unit=opt['units']['flow_unit'], use_nps=opt['use_nps'],
                       diameter_table={int(row[0]): float(row[1]) for row in opt['nps_dict']})
    gas_property = GasProperty(gravity=float(opt['constants']['ro']), temp=float(opt['constants']['t']), z=float(opt['constants'].get('Z', 1.0)), eff=float(opt['constants']['E']),
                               Tb_Pb=float(opt['constants']['t0']) / float(opt['constants']['p0']), viscosity=float(opt['constants']['vis']))
    Logger.instance().set_total(1, logger_type='file', file_address=self.request.id + '.out')
    try:
        pls = PipeLine.find_all_pipelines(points, streams, setting)
    except AssertionError as e:
        Logger.instance().log_done(True)
        return {'error': e.message}

    Logger.instance().set_total(len(pls), logger_type='file', file_address=self.request.id + '.out')
    flows = {}
    speeds = {}
    pressures = {}
    sections = {}
    for i, pl in enumerate(pls):
        solver = SteadySolver(pl, gas_property)
        Logger.instance().next_state()
        solver.new_method(Logger.instance().update_nonlin_progress)
        Logger.instance().next_state()
        solver.find_points_pressure()
        Logger.instance().next_state()

        result = pl.generate_response(setting)
        flows.update(result['flow'])
        speeds.update(result['speed'])
        pressures.update(result['pressure'])
        sections.update({p.id: i + 1 for p in pl.points})

    Logger.instance().log_done(False)
    return {'flows': flows, 'speeds': speeds, 'pressures': pressures, 'sections': sections}
